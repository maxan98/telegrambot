import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MaksSklyarov on 23.04.17.
 */
public class Keyboard {



    public static ReplyKeyboardMarkup getWeatherKeyboard(){

        KeyboardButton btn = new KeyboardButton();
        btn.setText("Сегодня");
        btn.setRequestLocation(true);
        KeyboardButton btn1 = new KeyboardButton();
        btn1.setText("На неделю");
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setSelective(true);
        keyboardMarkup.setResizeKeyboard(true);
        keyboardMarkup.setOneTimeKeyboad(false);
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardRow = new KeyboardRow();
        keyboardRow.add(btn);
        keyboardRow.add(btn1);
        keyboard.add(keyboardRow);
        keyboardMarkup.setKeyboard(keyboard);
        return keyboardMarkup;
    }
    public static ReplyKeyboardMarkup getDefaultKeyboard(){
        KeyboardButton btn = new KeyboardButton();
        btn.setText("Погода");
        KeyboardButton btn1 = new KeyboardButton();
        btn1.setText("Номер 2");
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setSelective(true);
        keyboardMarkup.setResizeKeyboard(true);
        keyboardMarkup.setOneTimeKeyboad(false);
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardRow = new KeyboardRow();
        keyboardRow.add(btn);
        keyboardRow.add(btn1);
        keyboard.add(keyboardRow);
        keyboardMarkup.setKeyboard(keyboard);
        return keyboardMarkup;
    }
}
