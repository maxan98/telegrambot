import java.sql.*;

/**
 * Created by MaksSklyarov on 23.04.17.
 */
public class Conn{
public static Connection conn;
public static Statement statmt;
public static ResultSet resSet;

// --------ПОДКЛЮЧЕНИЕ К БАЗЕ ДАННЫХ--------
public static void Conn() throws ClassNotFoundException, SQLException
        {
        conn = null;
        Class.forName("org.sqlite.JDBC");
        conn = DriverManager.getConnection("jdbc:sqlite:fbase.db");

        System.out.println(Bot.ANSI_RED+"База Подключена!");
        }

// --------Создание таблицы--------
public static void CreateDB(String tablename) throws ClassNotFoundException, SQLException
        {
        statmt = conn.createStatement();
        statmt.execute("CREATE TABLE if not exists '"+tablename+"' ('id' INTEGER PRIMARY KEY AUTOINCREMENT, 'Username' text, 'said' text,'answered' text,'chatid' text);");

        System.out.println(Bot.ANSI_RED+"Таблица создана или уже существует.");
        }

// --------Заполнение таблицы--------
public static void WriteDB(String s, String s2, String s3,String name,String idd) throws SQLException
        {
        //statmt.execute("INSERT INTO 'users' ('username', 'phone') VALUES ('Petya', 125453); ");
        //statmt.execute("INSERT INTO 'users' ('name', 'phone') VALUES ('Vasya', 321789); ");
        //statmt.execute("INSERT INTO 'users' ('name', 'phone') VALUES ('Masha', 456123); ");
            statmt.execute("INSERT INTO '"+name+"' ('Username','said','answered','chatid') VALUES ('"+s+"','"+s2+"','"+s3+"','"+idd+"')");

        System.out.println(Bot.ANSI_YELLOW_BACKGROUND+"Таблица заполнена");
        }

// -------- Вывод таблицы--------
public static void ReadDB() throws ClassNotFoundException, SQLException
        {
        resSet = statmt.executeQuery("SELECT * FROM users");

        while(resSet.next())
        {
        int id = resSet.getInt("id");
        String  name = resSet.getString("Username");
        String  said = resSet.getString("said");
        String  answer = resSet.getString("answered");
        System.out.println( "ID = " + id );
        System.out.println( "name = " + name );
        System.out.println( "said = " + said );
            System.out.println(answer);
        System.out.println();
        }

        System.out.println("Таблица выведена");
        }

// --------Закрытие--------
public static void CloseDB() throws ClassNotFoundException, SQLException
        {
        conn.close();
        statmt.close();
        resSet.close();

        System.out.println("Соединения закрыты");
        }

        }

