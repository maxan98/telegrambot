import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by MaksSklyarov on 24.04.17.
 */
public class Main extends JFrame {
    public JButton button1;
    public JTextField textField1;
    private JPanel panel1;
    private JTextField textField2;

    public Main(Bot bot){
        this.getContentPane().add(panel1);
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SendMessage sendMessage = new SendMessage().setChatId(textField2.getText());
                sendMessage.setText(textField1.getText());
                try {
                    bot.sendMessage(sendMessage);
                } catch (TelegramApiException e1) {
                    e1.printStackTrace();
                }
            }
        });

    }
}
