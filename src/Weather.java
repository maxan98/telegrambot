import org.openweathermap.api.UrlConnectionWeatherClient;
import org.openweathermap.api.WeatherClient;
import org.openweathermap.api.model.Coordinate;
import org.openweathermap.api.model.currentweather.CurrentWeather;
import org.openweathermap.api.query.Language;
import org.openweathermap.api.query.QueryBuilderPicker;
import org.openweathermap.api.query.UnitFormat;
import org.openweathermap.api.query.currentweather.ByGeographicCoordinates;
import org.openweathermap.api.query.currentweather.InCycle;


import java.util.List;

/**
 * Created by MaksSklyarov on 23.04.17.
 */
public class Weather  {
    private static final String API_KEY = "3b6cfd40d7114bbede9db60d8471f917";
    private WeatherClient weather = new UrlConnectionWeatherClient(API_KEY);

public String getWeather(String coord1, String coord2){
    Coordinate coord = new Coordinate(coord2,coord1);
    ByGeographicCoordinates inCycle = QueryBuilderPicker.pick()
            .currentWeather() .oneLocation().byGeographicCoordinates(coord)                                      // get current weather
               // get weather for 10 closest to coordinate cities
            .language(Language.RUSSIAN)                             // in English language
            .unitFormat(UnitFormat.METRIC)                          // in metric units
            .build();
CurrentWeather currentWeather = weather.getCurrentWeather(inCycle);
        return prettyPrint(currentWeather);

}
    private static String prettyPrint(CurrentWeather currentWeather) {
        return String.format(
                "Братка, у тебя в  %s(%s)не так уж и холодно, можно и на учебу сходить:\nТемпература: %.1f ℃\nВлажность: %.1f %%\nДавление: %.1f гПА\n",
                currentWeather.getCityName(), currentWeather.getSystemParameters().getCountry(),
                currentWeather.getMainParameters().getTemperature(),
                currentWeather.getMainParameters().getHumidity(),
                currentWeather.getMainParameters().getPressure()
        );
    }
}
