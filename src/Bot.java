import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.PhotosInterface;
import com.flickr4java.flickr.photos.SearchParameters;
import com.flickr4java.flickr.test.TestInterface;
import org.apache.http.HttpEntityEnclosingRequest;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by MaksSklyarov on 23.04.17.
 */
public class Bot extends TelegramLongPollingBot {
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static Long LastID;
    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi botsApi = new TelegramBotsApi();
        Bot bot = new Bot();
        Main mw = new Main(bot);

        mw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        mw.pack();
        mw.setVisible(true);
        try {

            botsApi.registerBot(bot);
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
        try {
            System.out.println(bot.getMe());

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
SendMessage start = new SendMessage();
        start.setReplyMarkup(Keyboard.getDefaultKeyboard());
        try {
            Conn.Conn();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasCallbackQuery()) {
            if (update.getCallbackQuery().equals("callback")) {
                SendMessage msg = new SendMessage().setChatId(update.getCallbackQuery().getMessage().getChatId());
                msg.setText("cb");
                try {
                    sendMessage(msg);
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }
        }
        if (update.hasMessage()) {
            SendMessage message = new SendMessage().setChatId(update.getMessage().getChatId());
            LastID = update.getMessage().getChatId();
            // CHECK IF LOCATION IS SENT TO US
            if (update.getMessage().hasLocation()) {


                message.setReplyMarkup(Keyboard.getDefaultKeyboard());


                System.out.println(ANSI_YELLOW+update.getMessage().getLocation());
                Weather weather = new Weather();
                String send = weather.getWeather(update.getMessage().getLocation().getLatitude().toString(), update.getMessage().getLocation().getLongitude().toString());
                message.setText(send);
               // System.out.println(send);
                try {
                    sendMessage(message);
                    Conn.CreateDB(update.getMessage().getFrom().getId().toString());
                    Conn.WriteDB(update.getMessage().getFrom().getUserName(),"location is"+update.getMessage().getLocation().toString(),message.getText(),update.getMessage().getFrom().getId().toString(),update.getMessage().getChatId().toString());
                    System.out.println(ANSI_GREEN+"DB Updated by "+update.getMessage().getFrom().getFirstName()+" "+update.getMessage().getFrom().getLastName()+" said"+update.getMessage().getText());
                    System.out.println(ANSI_RED+update.getMessage().getChatId());
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                return;


            }


            // CHECK NA TEXT
            if (update.getMessage().hasText()) {
                if (update.getMessage().getText().equals("Погода")) {
                    message.setReplyMarkup(Keyboard.getWeatherKeyboard());
                    message.setText("Желаете узнать текущую погоду или прогноз на неделю?");
                }
                //ЗАГЛУШКА
                else {
                    message.setReplyMarkup(Keyboard.getDefaultKeyboard());
                    Flickr f = new Flickr("514118b178a1014d775dab58156551c0", "d292d65ecc0d0b69", new REST());
                    PhotosInterface testInterface = f.getPhotosInterface();
                    SearchParameters parameters = new SearchParameters();
                   // parameters.setTags(new String[]{"boobs", "naked"});
                    parameters.setText("cat");
                    parameters.setAccuracy(90);
                    parameters.setTags(new String[]{"animals"});
                    try {

                        PhotoList<Photo> list = testInterface.search(parameters, 500, 5);
                        Photo photo = list.get(new Random().nextInt(500));
                        message.setText(photo.getLarge1600Url()+ "\n Я не понимаю, что ты имел ввиду:( На посмотри на картинку:) Надеюсь тебе попались котики :)");

                       //Conn.WriteDB(update.getMessage().getFrom().getUserName(),update.getMessage().getText(),photo.getLarge1600Url(),update.getMessage().getFrom().getUserName());
                        //Conn.WriteDB("ew","ew3","242");
//                        Conn.ReadDB();



                    } catch (FlickrException e) {
                        e.printStackTrace();
                    }
                    //message.setText("Не понимаю тебя, друг..");}
                }

                //OTPRAVKA SMS
                try {
                    String username;

                    Conn.CreateDB(update.getMessage().getFrom().getId().toString());
                    Conn.WriteDB(update.getMessage().getFrom().getUserName(),update.getMessage().getText(),message.getText(),update.getMessage().getFrom().getId().toString(),update.getMessage().getChatId().toString());
                    System.out.println(ANSI_GREEN+"DB Updated by "+update.getMessage().getFrom().getFirstName()+" "+update.getMessage().getFrom().getLastName()+" said: "+update.getMessage().getText());
                    sendMessage(message);
                    System.out.println(ANSI_RED+update.getMessage().getChatId());

                } catch (TelegramApiException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }
    }
//CUT OUT API KEYS HttpEntityEnclosingRequest
    @Override
    public String getBotUsername() {
        return "SPBGUAPBOT";
    }

    @Override
    public String getBotToken() {
        return "";
    }

}


